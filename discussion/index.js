/*
log in the console the ff gwa per subject of a student:
98.5
94.3
89.2
90.0
*/

let grades = [98.5,94.3,89.2,90.0];
console.log(grades);
// ***** ++OUTPUT++ *****
// (4)[98.5, 94.3, 89.2, 90]
//===================================

/*
	OBJECTS
		------>  collection of related data & functionalities

*/
let grade = {
	// curly braces - initializer for creating objects
	// key - value pair
		/*
			key - field/identifier/property to describe the data/value;
			value - info that serves as the value of the key/field
		*/
	math: 98.5,
	english: 94.3,
	science: 90.0,
	MAPEH: 89.2
};
console.log(grade);
// ********* ++OUTPUT++ ***************
// {math: 98.5, english: 94.3, science: 90, MAPEH: 89.2}
//==========================================


/*
ACCESSING ELEMENTS INSIDE THE OBJECT
	SYNTAX FOR CREATING AN OBJ
		let objectName = {
			keyA:valueA,
			keyB:valueB
		}

	Dot Notation - used to access the specific property & value of the object;preferred in terms of 
	obj access
	.....
*/

// accessing 1 key in the obj
console.log(grade.english);
// ********* ++OUTPUT++ ***************
// 			94.3
//=====================================

// accessing 2 keys in the obj
console.log(grade.english,grade.math);
// ********* ++OUTPUT++ ***************
// 			94.3 98.5
//=====================================

/*
create a cellphone obj that has the ff keys (supply your own values for each)
	brand
	color
	mfd
take a screenshot of your output & send it to our 
*/


let cellphone = {
	
	brand: "pixel",
	color: "black",
	mfd: 2020,
};
console.log(cellphone);
// ********* ++OUTPUT++ ***************
// {brand: 'pixel', color: 'black', mfd: 2020}	
//=====================================

// typeof det the data type of the element
console.log(typeof cellphone);
// ********* ++OUTPUT++ ***************
// 			object
//=====================================


// NESTED ELEMENTS
let student = {
	firstName: "John",
	lastName: "Smith",
	mobileNo: 09123456789,
	location:{
		city:"Tokyo",
		country: "Japan",
	},
	email:["john@mail.com", "john.smith@mail.com"],

	fullName:(function name(fullName) { // name is optional
		// this - refers to the object where it is inserted
		/*
			in this case, the code below could be typed as:
				console.log(student.firstName + " " + student.lastName);
		*/
		console.log(this.firstName + " " + this.lastName)  // for efficient coding
	})
};
//console.log(student.firstName + "" + student.lastName);
student.fullName();
// ********* ++OUTPUT++ ***************
// 			John Smith
//=====================================

console.log(student.location.city);
// ********* ++OUTPUT++ ***************
// 			Tokyo
//=====================================
console.log(student.email);  // EMAIL IS A FORM OF AN ARRAY
// *************** ++OUTPUT++ ********************
// 	(2)['john@mail.com', 'john.smith@mail.com']		
//================================================
console.log(student.email[0]);
// ********* ++OUTPUT++ ***************
// 			john@mail.com
//=====================================
console.log(student.email[1]);
// ********* ++OUTPUT++ ***************
// 			john.smith@mail.com
//=====================================


/*
ARRAY OF OBJECTS
*/

let contactList = [  //array containing an obj
{   //1st INDEX 0 /element
	firstName: "John",
	lastName: "Smith",
	location: "Japan",
},

{	//2nd INDEX 1
	firstName: "Jane",
	lastName: "Smith",
	location: "Japan",
},

{	//3rd INDEX 2
	firstName: "Jasmine",
	lastName: "Smith",
	location: "Japan",
},
];



// accessing an element inside the array
console.log(contactList[1]); 
// ********* ++OUTPUT++ ***************
// 			Object
// 		firstName: "Jane"
//		lastName: "Smith" 
// 		location: "Japan"
//=====================================
// accessing a key in an obj that is inside the array
console.log(contactList[2].firstName); // [] ARRAY;  . OBJ; 
// ********* ++OUTPUT++ ***************
// 			Jasmine
//=====================================

/*
Constructor Function - create reuseable function to create sev objects
					 - useful for creating copies/instances of an obj
		SYNTAX:
			function objectName (keyA,keyB){
				this.keyA = keyA,
				this.keyB = keyB
			}
*/
/*
	obj literals - let objectName = {}

	instance - concrete occurence of any obj w/c emphasizes on the unique identity of it
*/

// Reuseable Function/Obj Method
// the function name will serve as the obj name for every instance



//.......continue later ... line 130
function Laptop(name, manufacturedDate){  // Laptop -- obj name
	this.name=name,
	this.manufacturedDate = manufacturedDate

	/*
		it also performs diff commands
		console.log(this)
	*/
};
// new keyword refers/signifies that there will be a new obj under the Laptop function
let laptop1 = new Laptop("Lenovo", 2008);
console.log(laptop1);

let laptop2 = new Laptop("Toshiba", 1997);
console.log(laptop2);

/*
	create a var & store the laptop1 & laptop2 in it (the resulting output must be an array)
	log the array in the console
*/

let array = [laptop1, laptop2];
console.log(array); 

/*
initializing, adding, deleting, reassigning of obj properties
*/

let car = {};
console.log(car);

// adding object properties

car.name = "Honda Civic"; // if 170&171 del, car would be empty; also applic for del & reassign
console.log(car);

car["manufacturedDate"] = 2020; // also applic for del & reassign
console.log(car);

// reassigning obj (same/existing properties, but assigning a diff value)
car.name = "volvo";   // no 2 keys w/ the same name but it can be updated
console.log(car);

//deleting obj properties
// delete car.manufacturedDate;
// console.log(car);

delete car['manufacturedDate'];
console.log(car);

let person = {
	name: "John",
	talk: function(){
		console.log('Hi! My name is' + this.name)
	}
}

// add property
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}


//=================================
//=================================
//=================================


let friend = {  //
	firstName: "Joe",
	lastName: "Doe",
	address:{
		city: "Austin",
		state: "Texas"
	},
	emails:["joe@mail.com", "joedoe@mail.com"], // form of array, access w/ . ; spec email w/ [] since it is an OBJ
	introduce: function(){   // form of obj
		console.log("Hello! My name is "+ this.firstName + " " + this.lastName)
	}
};

let pokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function (){   // pokemon.tackle
		console.log("This pokemon tackeld targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
}
//pokemon.tackle()
//This pokemon tackeld targetPokemon
//targetPokemon's health is now reduced to targetPokemonHealth



function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level
	// Skills of Pokemon
	this.tackle = function(target){  // target must be existing Pokemon, e.g. Charizard, else, error will be recvd
			//pokemon.tackle(raichu)   ----  assuming raichu not yet created
			//VM259:1 Uncaught ReferenceError: raichu is not defined
    at <    //nonymous>:1:16
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	}
	this.faint = function(target){   
		console.log(this.name + " fainted.")
	}
}
// Pikachu.faint()
// Pikachu fainted.



let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 8);
// Pikachu.tackle(Charizard) 
//Pikachu tackled Charizard
//targetPokemon's health is now reduced to targetPokemonHealth

let Balbasaur = new Pokemon("Bulbasaur", 9);
let Raichu = new Pokemon("Raichu", );   //......











