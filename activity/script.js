//console.log("helloworld");


let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {hoenn: [ "May", "Max" ], kanto: [ "Brock", "Misty"]}, 
	talk: function(){
		console.log('Pikachu! I choose you!')
	}
};

// =========================================

console.log('Result of dot notation:');
console.log(trainer.name);
console.log(trainer.age);
console.log(trainer.pokemon);
console.log(trainer.friends);
console.log(trainer.friends.hoenn[1]);
console.log(trainer.friends.kanto[0]);

console.log('Result of square bracket notation:');
console.log(trainer.pokemon[0]);
console.log(trainer.friends.hoenn[1]);
console.log(trainer.friends.kanto[0]);

console.log('Result of talk method');
console.log(trainer.talk());

// =========================================

function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level

	this.tackle = function(target){  
		console.log(this.name + " tackled " + target.name + "!");
		let targetPokemonNewHealth = (target.health - this.attack);
		console.log(target.name + "'s  health is now reduced to " + targetPokemonNewHealth);
		if (targetPokemonNewHealth <= 0) {
				function faint() {
					return `${target.name} fainted`
			}
				var result = faint(target.name);
				console.log(result);
		}
	}
}

// =========================================

let Pikachu = new Pokemon ("Pikachu", 65);
let Charizard = new Pokemon ("Charizard", 36);
let Squirtle = new Pokemon ("Squirtle", 16);
let Bulbasaur = new Pokemon ("Bulbasaur", 16);
let Charmander = new Pokemon("Charmander", 16);
let Electabuzz = new Pokemon("Electabuzz", 30);
let Jigglypuff = new Pokemon ("Jigglypuff", 34);
let Buizel = new Pokemon("Buizel", 26);

// =========================================
// Pikachu.tackle(Charmander)
// Pikachu tackled Charmander!
// script.js:43 Charmander's  health is now reduced to -33
// script.js:49 Charmander fainted





